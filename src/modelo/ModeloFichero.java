package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ModeloFichero implements IModelo {
        File alumnos = new File("FicheroAlumnos.csv"); //creamos el archivo para guardar los alumnos
        File cursos = new File("FicheroCursos.csv"); //creamos el archivo para guardar los cursos
        File matriculas = new File("FicheroMatriculas.csv"); //creamos el archivo para guardar las matrículas
    
    public ModeloFichero() throws IOException {
        FileWriter filewriter;
        if (!alumnos.exists()) {
            filewriter = new FileWriter(alumnos);
            filewriter.close();
        }
        if (!cursos.exists()) {
            filewriter = new FileWriter(cursos);
            filewriter.close();
        }
        if (!matriculas.exists()) {
            filewriter = new FileWriter(matriculas);
            filewriter.close();
        }
    }
    
    @Override
    public void create(Alumno alumno) {
        try {
            FileWriter filewriter = new FileWriter(alumnos, true); //creamos un objeto filewriter para utilizarlo sobre el fichero de alumnos
            filewriter.write(alumno.getId() + ";" + alumno.getNombre() + ";" + alumno.getDni() + ";" + alumno.getEdad() 
                + ";" + alumno.getEmail() + ";\r\n"); //escribimos el objeto en el fichero separando los datos con ;
            filewriter.close(); //cerramos el fichero de alumnos
        } catch (IOException e) { //capturamos la excepción 
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, e); //mostramos el error
        }
    }
    
    @Override
    public void create(Curso curso) {
        try {
            FileWriter filewriter = new FileWriter(cursos, true); //creamos un objeto filewriter para utilizarlo sobre el fichero de cursos
            filewriter.write(curso.getIdCurso() + ";" + curso.getNombre() + ";" 
                + curso.getHoras() + ";\r\n"); //escribimos el objeto en el fichero separando los datos con ;
            filewriter.close(); //cerramos el fichero de cursos
        } catch (IOException ex) { //capturamos la excepción 
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex); //mostramos el error
        }
    }
    
    @Override
    public void create(Matricula matricula) {
        try {
            FileWriter filewriter = new FileWriter(matriculas, true); //creamos un objeto filewriter para utilizarlo sobre el fichero matriculas
            filewriter.write(matricula.getIdMatricula() + ";" + matricula.getAlumno().getId()+ ";" + matricula.getAlumno().getNombre() + ";" 
                + matricula.getAlumno().getDni() + ";" + matricula.getAlumno().getEdad() + ";" + matricula.getAlumno().getEmail() + ";" 
                + matricula.getCurso().getIdCurso() + ";" + matricula.getCurso().getNombre() + ";" 
                + matricula.getCurso().getHoras() + ";\r\n"); //escribimos el objeto en el fichero separando los datos con ;
            filewriter.close(); //cerramos el fichero de alumnos
        } catch (IOException ex) { //capturamos la excepción 
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex); //mostramos el error
        } 
    }
    
    @Override
    public HashSet<Alumno> readAlumno() {
        HashSet hashset = new HashSet();
        try {
            FileReader filereader = new FileReader(alumnos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                String dni = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                Alumno alumno = new Alumno(idAlumno, nAlumno, dni, edad, email);
                hashset.add(alumno);
                string = bufferedreader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashset;
    }
    
    @Override
    public HashSet<Curso> readCurso() {
        HashSet hashset = new HashSet();
        try {
            FileReader filereader = new FileReader(cursos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
            StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idCurso = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();
                int horas = Integer.parseInt(stokenizer.nextToken());
                Curso curso = new Curso(idCurso, nombre, horas);
                hashset.add(curso);
                string = bufferedreader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashset;
    }
    
    @Override
    public HashSet<Matricula> readMatricula() {
        HashSet hashset = new HashSet();
        try {
            FileReader filereader = new FileReader(matriculas);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                //Recogemos las variables del objeto matricula
                String idMatricula = stokenizer.nextToken();
                //Recogemos las variables del objeto alumno
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                String dni = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                //Recogemos las variables del objeto curso
                String idCurso = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();
                int horas = Integer.parseInt(stokenizer.nextToken());
                Alumno alumno = new Alumno(idAlumno, nAlumno, dni, edad, email);
                Curso curso = new Curso(idCurso, nombre, horas);
                Matricula matricula = new Matricula(idMatricula, curso, alumno);
                hashset.add(matricula);
                string = bufferedreader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashset;
    }

    @Override
    public void update(Alumno alumno) {
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(alumnos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                String dni = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                Alumno a = new Alumno(idAlumno, nAlumno, dni, edad, email);
                if (alumno.getId().equals(a.getId())) {
                    filewriter.write(alumno.getId() + ";" + alumno.getNombre() + ";" + alumno.getDni() + ";" + alumno.getEdad() + ";" 
                        + alumno.getEmail() + ";\r\n"); 
                } else {
                    filewriter.write(a.getId() + ";" + a.getNombre() + ";" + a.getDni() + ";" + a.getEdad() + ";" 
                        + a.getEmail() + ";\r\n");
                }
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        alumnos.delete();
        boolean rename = temp.renameTo(new File("FicheroAlumnos.csv"));
    }
    
    @Override
    public void update(Curso curso) {
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(cursos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idCurso = stokenizer.nextToken();
                String nCurso = stokenizer.nextToken();
                int horas = Integer.parseInt(stokenizer.nextToken());
                Curso c = new Curso(idCurso, nCurso, horas);
                if (curso.getIdCurso().equals(c.getIdCurso())) {
                    filewriter.write(curso.getIdCurso() + ";" + curso.getNombre() + ";" + curso.getHoras() + ";\r\n"); 
                } else {
                    filewriter.write(c.getIdCurso() + ";" + c.getNombre() + ";" + c.getHoras() + ";\r\n");
                }
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        cursos.delete();
        boolean rename = temp.renameTo(new File("FicheroCursos.csv"));
    }
    
    @Override
    public void update(Matricula matricula) {
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(matriculas);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                //Recogemos las variables del objeto matricula
                String idMatricula = stokenizer.nextToken();
                //Recogemos las variables del objeto alumno
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                String dni = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                //Recogemos las variables del objeto curso
                String idCurso = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();
                int horas = Integer.parseInt(stokenizer.nextToken());
                Alumno alumno = new Alumno(idAlumno, nAlumno, dni, edad, email);
                Curso curso = new Curso(idCurso, nombre, horas);
                Matricula m = new Matricula(idMatricula, curso, alumno);
                if (matricula.getIdMatricula().equals(m.getIdMatricula())) {
                    filewriter.write(matricula.getIdMatricula() + ";" + matricula.getAlumno().getId()+ ";" + matricula.getAlumno().getNombre() + ";" 
                + matricula.getAlumno().getDni() + ";" + matricula.getAlumno().getEdad() + ";" + matricula.getAlumno().getEmail() + ";" 
                + matricula.getCurso().getIdCurso() + ";" + matricula.getCurso().getNombre() + ";" 
                + matricula.getCurso().getHoras() + ";\r\n");
                } else {
                    filewriter.write(m.getIdMatricula() + ";" + m.getAlumno().getId()+ ";" + m.getAlumno().getNombre() + ";" 
                + m.getAlumno().getDni() + ";" + m.getAlumno().getEdad() + ";" + m.getAlumno().getEmail() + ";" 
                + m.getCurso().getIdCurso() + ";" + m.getCurso().getNombre() + ";" + m.getCurso().getHoras() + ";\r\n");
                }
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        matriculas.delete();
        boolean rename = temp.renameTo(new File("FicheroMatriculas.csv"));
    }

    @Override
    public void delete(Alumno alumno) {
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(alumnos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                String dni = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                Alumno a = new Alumno(idAlumno, nAlumno, dni, edad, email);
                if (!alumno.getId().equals(a.getId())) {
                    filewriter.write(a.getId() + ";" + a.getNombre() + ";" + a.getDni() + ";"+ a.getEdad() + ";" + a.getEmail() + ";\r\n");
                } 
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        alumnos.delete();
        boolean rename = temp.renameTo(new File("FicheroAlumnos.csv"));
    }

    @Override
    public void delete(Curso curso) {
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(cursos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idCurso = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();
                int horas = Integer.parseInt(stokenizer.nextToken());
                Curso c = new Curso(idCurso, nombre, horas);
                if (!curso.getIdCurso().equals(c.getIdCurso())) {
                    filewriter.write(c.getIdCurso() + ";" + c.getNombre()+ ";" + c.getHoras() + ";\r\n");
                } 
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        cursos.delete();
        boolean rename = temp.renameTo(new File("FicheroCursos.csv"));
    }
    
    @Override
    public void delete(Matricula matricula) {
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(matriculas);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                //Recogemos las variables del objeto matricula
                String idMatricula = stokenizer.nextToken();
                //Recogemos las variables del objeto alumno
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                String dni = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                //Recogemos las variables del objeto curso
                String idCurso = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();
                int horas = Integer.parseInt(stokenizer.nextToken());
                Alumno alumno = new Alumno(idAlumno, nAlumno, dni, edad, email);
                Curso curso = new Curso(idCurso, nombre, horas);
                Matricula m = new Matricula(idMatricula, curso, alumno);
                if (!matricula.getIdMatricula().equals(m.getIdMatricula())) {
                    filewriter.write(m.getIdMatricula() + ";" + m.getAlumno().getId()+ ";" + m.getAlumno().getNombre() + ";" 
                + m.getAlumno().getDni() + ";" + m.getAlumno().getEdad() + ";" + m.getAlumno().getEmail() + ";" 
                + m.getCurso().getIdCurso() + ";" + m.getCurso().getNombre() + ";" + m.getCurso().getHoras() + ";\r\n");
                } 
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        matriculas.delete();
        boolean rename = temp.renameTo(new File("FicheroMatriculas.csv"));
    }
}