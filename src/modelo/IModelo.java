package modelo;

import java.util.HashSet;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public interface IModelo {
    	//Alumno
	public void create(Alumno alumno);
	public HashSet<Alumno> readAlumno();
	public void update(Alumno alumno);
	public void delete(Alumno alumno);
	
	//Curso
	public void create(Curso curso);
	public HashSet<Curso> readCurso();
	public void update(Curso curso);
	public void delete(Curso curso);
	
	//Matricula
	public void create(Matricula matricula);
	public HashSet<Matricula> readMatricula();
	public void update(Matricula matricula);
	public void delete(Matricula matricula);
}