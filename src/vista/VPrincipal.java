package vista;

import controlador.Configurador;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class VPrincipal {
    public static int menuInicial() {
        int opt;

        System.out.println("\n***************************");
        System.out.println("* MENÚ PRINCIPAL");
        System.out.println("* -------------------------");
        System.out.println("* 0. Salir");
        System.out.println("* -------------------------");
        System.out.println("* 1. Alumno");
        System.out.println("* 2. Curso");
        System.out.println("* 3. Matrícula");
        System.out.println("***************************");
        System.out.println("\nEscoge una opción: ");
        opt = leerNumero();
        return opt;
    }
    
    public static char menuEstructura() {
        String nombre = Configurador.getNombreApp();
        char opt;
        
        System.out.println("\n" + nombre);
        System.out.println("\n********************************");
        System.out.println("* ESCOGE EL TIPO DE ESTRUCTURA");
        System.out.println("* ------------------------------");
        System.out.println("* s. Salir");
        System.out.println("* ------------------------------");
        System.out.println("* a. Array");
        System.out.println("* h. HashSet");
        System.out.println("* f. Fichero CSV");
        System.out.println("********************************");
        System.out.println("\nEscoge una opción: ");
        opt = leerCaracter();
        return opt;
    }
    
    public static char menuCrud() {
        char opt;
        System.out.println("\n***************************");
        System.out.println("* MENÚ C.R.U.D.");
        System.out.println("* -------------------------");
        System.out.println("* e. Salir (exit)");
        System.out.println("* -------------------------");
        System.out.println("* c. Crear (create)");
        System.out.println("* r. Leer (read)");
        System.out.println("* u. Actualizar (update)");
        System.out.println("* d. Borrar (delete)");
        System.out.println("* -------------------------");
        System.out.println("* s. Buscar (search)");
        System.out.println("***************************");
        System.out.println("\nEscoge una opción: ");
        opt = leerCaracter();
        return opt;
    }
    
    public static void mostrarTexto(String texto) {
        System.out.println(texto);
    }
    
    public static void mostrarTextoError(String texto) {
        System.err.println(texto);
    }
    
    public static String leerTexto() {
        Scanner scanner = new Scanner(System.in); 
        String texto = scanner.nextLine();
        return texto;
    }
    
    public static int leerNumero() {
        Scanner scanner = new Scanner(System.in); 
        int numero = scanner.nextInt();
        return numero;
    }
    
    public static char leerCaracter() {
        Scanner scanner = new Scanner(System.in);
        String cadena = scanner.next().toLowerCase();
        char caracter = cadena.charAt(0);
        return caracter;
    }
        
    public static boolean validarEmail(String s) {
        Pattern pattern = Pattern.compile("[\\w\\.\\d]+@\\w+\\.\\w+");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
    
    public static boolean validarDNI (String s) {
        Pattern pattern = Pattern.compile("(([X-Z]{1})([-])(\\d{7})([-])([A-Z]{1}))|((\\d{8})([-])([A-Z]{1}))");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
    
    public static boolean validarLetra (String s) { 
        char letras[] = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
        if (s.length() < 11) {
            char letra = s.charAt(9);
            String num = s.substring(0,8);
            int numeros = Integer.parseInt(num);
            return letra == letras[numeros%23];
        } else {
            return true;
        }
    }
    
    public static void errorNum() {
        System.err.println("\nEl valor introducido no es numérico. Por favor, introduce un número.");
    }
    
    public static void errorMenu() {
        System.err.println("\nEscoge una de las opciones del menú.");
    }
    
    public static void despedir() {
        System.out.println("\n¡Hasta luego! Cerrando aplicación...");
    }

    public static void despedirCRUD() {
        System.out.println("\nVolviendo al menú principal...");
    }
}