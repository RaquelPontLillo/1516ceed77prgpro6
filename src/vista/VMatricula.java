package vista;

import modelo.Matricula;
import modelo.Alumno;
import modelo.Curso;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class VMatricula implements IVista<Matricula> {
  private String idMatricula;
  private Alumno alumno;
  private Curso curso;
 
    public Matricula alta() {
        System.out.println("\nINTRODUCIR DATOS");
        System.out.println("Introduce el código la matrícula: ");
        idMatricula = VPrincipal.leerTexto();

        Matricula matricula = new Matricula (idMatricula);
        return matricula;
    }
  
    public void mostrar(Matricula matricula) {
        VAlumno va = new VAlumno();
        VCurso vc = new VCurso();
        System.out.println("\n********************************************************************************");
        System.out.println("*  JUSTIFICANTE DE MATRÍCULA ");
        System.out.println("* ------------------------------------------------------------------------------ ");
        System.out.println("* Código de matrícula: " + matricula.getIdMatricula());
        System.out.println("*");
        va.mostrar(matricula.getAlumno());
        System.out.println("*");
        vc.mostrar(matricula.getCurso());
        System.out.println("********************************************************************************");
        Date fecha = new Date();
        System.out.println("\nJustificante generado el: " + fecha + " || SIN VALIDEZ ACADÉMICA");
        System.out.println("\n--------------------------------------------------------------------------------");
    }

    public void mostrarVarios(HashSet hashset) {
        IVista<Matricula> vm = new VMatricula();
        Iterator iterator = hashset.iterator();
        Matricula matricula = null;

        while (iterator.hasNext()) {
            matricula = (Matricula) iterator.next();
            vm.mostrar(matricula);
            System.out.println("\n\n");
        }
    }
}