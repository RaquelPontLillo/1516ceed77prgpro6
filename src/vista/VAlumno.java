package vista;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import modelo.Alumno;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class VAlumno implements IVista <Alumno> {
    private int edad;
    private String email;
    private String idPersona;
    private String nombre;
    private String dni;
    
    public Alumno alta() throws InputMismatchException {
        System.out.println("\nINTRODUCIR DATOS");
        System.out.println("Introduce el código del alumno: ");
        idPersona = VPrincipal.leerTexto();
        System.out.println("Introduce el nombre del alumno: ");
        nombre = VPrincipal.leerTexto();
        
        Boolean error = true;
        Boolean valido;
        
        do {
            System.out.println("Introduce el DNI (ej: 11223344-A) o NIE (ej: X-1223344-A) del alumno: ");
            dni = VPrincipal.leerTexto();
            valido = VPrincipal.validarDNI(dni);
            if (valido) {
                Boolean letra = VPrincipal.validarLetra(dni);
                if (letra == false) {
                    System.err.println("\nLa letra que has introducido no concuerda con el número de DNI.");
                } else {
                    error = false;
                }
            } else {
                System.err.println("\nEl DNI o NIE introducido no es válido.");
            }
        } while (error);
        
        error = true;
        
        do {
            System.out.println("Introduce el e-mail del alumno: ");
            email = VPrincipal.leerTexto();
            valido = VPrincipal.validarEmail(email);
            if (valido) {
                error = false;
            } else {
                System.err.println("\nEl e-mail introducido no es válido.");
            }
        } while (error);
        
        error = true;

        do {
            try {
                System.out.println("Introduce la edad del alumno: ");
                edad = VPrincipal.leerNumero();
                error = false;
            } catch (InputMismatchException e) {
                VPrincipal.errorNum();
          }
        } while (error);

        Alumno alumno = new Alumno (idPersona, nombre, dni, edad, email);
        return alumno;
  }
   
    public void mostrar(Alumno alumno) {
        System.out.println("* DATOS PERSONALES DEL ALUMNO");
        System.out.println("* ------------------------------------------------------------------------------");
        System.out.println("* Código personal: " + alumno.getId());
        System.out.println("* Nombre: " + alumno.getNombre() + "      DNI: " + alumno.getDni() + "      Edad: " + alumno.getEdad() + " años");
        System.out.println("* Correo electrónico: " + alumno.getEmail());
   }
    
    public void mostrarVarios(HashSet hashset) {
        IVista<Alumno> va = new VAlumno();
        Iterator iterator = hashset.iterator();
        Alumno alumno = null;

        while (iterator.hasNext()) {
            System.out.println("\n********************************************************************************");
            alumno = (Alumno) iterator.next();
            va.mostrar(alumno);
            System.out.println("********************************************************************************");
        }
    }
}
