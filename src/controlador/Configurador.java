package controlador;

import vista.VPrincipal;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Configurador {
    private static Configurador instancia;
    private static String nameApp;

    public static String getNombreApp() {
        return nameApp;
    }

    private Configurador(String nameApp) {
        this.nameApp = nameApp;
    }

    public static Configurador getInstanciaApp(String nameApp) {
        if (instancia == null) {
            instancia = new Configurador(nameApp);
            VPrincipal.mostrarTexto("\nEjecutando aplicación, espere...");
        } else {
            VPrincipal.mostrarTextoError("\n¡Error! Únicamente se permite una instancia de la aplicación.");
        }
        return instancia;
    }
}
