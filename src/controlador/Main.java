package controlador;

import java.io.IOException;
import vista.VPrincipal;
import modelo.IModelo;
import java.util.InputMismatchException;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Main {
    public static void main (String[] args) throws InputMismatchException, IOException { 
        IModelo modelo = null;
        VPrincipal vista = new VPrincipal();
        CPrincipal controlador = new CPrincipal (modelo, vista);
    }
}