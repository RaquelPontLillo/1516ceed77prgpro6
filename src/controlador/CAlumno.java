package controlador;

import java.util.HashSet;
import java.util.Iterator;
import modelo.Alumno;
import modelo.IModelo;
import modelo.Matricula;
import vista.IVista;
import vista.VAlumno;
import vista.VPrincipal;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class CAlumno {
    private IModelo modelo;
    
    public CAlumno(IModelo m) {
        modelo = m;
        Boolean bucle = true;
        do {
            char opcion = VPrincipal.menuCrud();
            switch (opcion) {
                case 'e':
                    VPrincipal.despedirCRUD();
                    return;
                case 'c':
                    create();
                    break;
                case 'r':
                    read();
                    break;
                case 'u':
                    update();
                    break;
                case 'd':
                    delete();
                    break;
                case 's':
                    search();
                    break;
                default:
                    VPrincipal.errorMenu();
                    break;
            }
        } while (bucle);
    }
    
    private void create() {
        IVista<Alumno> va = new VAlumno();
        Alumno alumno = va.alta();
        HashSet alumnos = modelo.readAlumno();
        Iterator iterator = alumnos.iterator();
        
        while (iterator.hasNext()) {
            Alumno a = (Alumno) iterator.next();
            if (alumno.getId().equals(a.getId())) {
                VPrincipal.mostrarTextoError("\nYa existe un alumno con el código proporcionado.");
                return;
            }
        }
        modelo.create(alumno);
        VPrincipal.mostrarTexto("\nAlumno dado de alta con éxito.");
    }
    
    private void read() {
        VAlumno va = new VAlumno();
        HashSet hashset = modelo.readAlumno();
        va.mostrarVarios(hashset);
    }
    
    private void update() {
        VPrincipal.mostrarTexto("\nEscribe el código del alumno que quieres modificar: ");
        String id = VPrincipal.leerTexto();
        Alumno alumno = null;
        HashSet alumnos = modelo.readAlumno();
        Iterator iterator = alumnos.iterator();
        while (iterator.hasNext()) {
            Alumno a = (Alumno) iterator.next();
            if (id.equals(a.getId())) {
                alumno = a;
            }
        }
        if (alumno != null) {
            HashSet matriculas = modelo.readMatricula();
            iterator = matriculas.iterator();
            while (iterator.hasNext()) {
                Matricula m = (Matricula) iterator.next();
                if (alumno.equals(m.getAlumno())) {
                    VPrincipal.mostrarTextoError("\nEl alumno que quieres actualizar tiene una matrícula asociada. "
                            + "Debes borrar primero la matrícula, actualizarlo y luego volver a crearla.");
                    return;
                }
            }
            IVista<Alumno> va = new VAlumno();
            alumno = va.alta();
            alumno.setId(id);
            modelo.update(alumno);
            VPrincipal.mostrarTexto("\nAlumno actualizado con éxito.");
        } else {
            VPrincipal.mostrarTextoError("\nEl código del alumno proporcionado no existe.");
        }
    }
    
    private void delete() {
        VPrincipal.mostrarTexto("\nEscribe el código del alumno que quieres borrar: ");
        String id = VPrincipal.leerTexto();
        Alumno alumno = null;
        HashSet alumnos = modelo.readAlumno();
        Iterator iterator = alumnos.iterator();
        while (iterator.hasNext()) {
            Alumno a = (Alumno) iterator.next();
            if (id.equals(a.getId())) {
                alumno = a;
            }
        }
        if (alumno != null) {
            HashSet matriculas = modelo.readMatricula();
            iterator = matriculas.iterator();
            while (iterator.hasNext()) {
                Matricula m = (Matricula) iterator.next();
                if (alumno.equals(m.getAlumno())) {
                    VPrincipal.mostrarTextoError("\nEl alumno que quieres borrar tiene una matrícula asociada. "
                            + "Debes borrar primero la matrícula.");
                    return;
                }
            }
        modelo.delete(alumno);
        VPrincipal.mostrarTexto("\nAlumno borrado con éxito.");
        } else {
            VPrincipal.mostrarTextoError("\nEl código del alumno proporcionado no existe.");
        }
    }
    
    private void search() {
        IVista<Alumno> va = new VAlumno();
        Alumno alumno = null;
        VPrincipal.mostrarTexto("\nIntroduce el código del alumno que quieres buscar: ");
        String id = VPrincipal.leerTexto();
        HashSet alumnos = modelo.readAlumno();
        Iterator iterator = alumnos.iterator();
        
        while (iterator.hasNext()) {
            Alumno a = (Alumno) iterator.next();
            if (id.equals(a.getId())) {
                alumno = a;
            }
        }
        if (alumno != null) {
            VPrincipal.mostrarTexto("\n********************************************************************************");
            va.mostrar(alumno);
            VPrincipal.mostrarTexto("********************************************************************************");
        } else {
            VPrincipal.mostrarTextoError("\nEl código del alumno proporcionado no existe.");
        }
    }
}